package br.com.Mentorama.Produtos;

public class Produtos {
    private Integer id;
    private String nome;
    private double valor;
    private double desconto;
    private double quantidadenoestoque;
    private Integer codigodoproduto;

    public Produtos(final Integer id,final String nome, final double valor,final double desconto, final double quantidadenoestoque, final Integer codigodoproduto){
    this.id =  id;
    this.nome = nome;
    this.valor = valor;
    this.desconto = desconto;
    this.quantidadenoestoque = quantidadenoestoque;
    this.codigodoproduto = codigodoproduto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public double getQuantidadenoestoque() {
        return quantidadenoestoque;
    }

    public void setQuantidadenoestoque(double quantidadenoestoque) {
        this.quantidadenoestoque = quantidadenoestoque;
    }

    public Integer getCodigodoproduto() {
        return codigodoproduto;
    }

    public void setCodigodoproduto(Integer codigodoproduto) {
        this.codigodoproduto = codigodoproduto;
    }

    public double CalcularValorDesconto(double valor, double desconto){
        return valor*desconto;
    }

    public double CalcularQuantidadeComprada(double quantidadenoestoque, double valor){
        return quantidadenoestoque*valor;
    }
}
