package br.com.Mentorama.Produtos;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/produtos")
public class ProdutosController {

    private final List<Produtos> produto;

    public ProdutosController(){
        this.produto = new ArrayList<Produtos>();
        Produtos produto1 = new Produtos(01,"Geladeira", 110,0.5, 50, 746298);
        Produtos produto2 = new Produtos(02,"Cama",80,0.2,50,658234);
        Produtos produto3 = new Produtos(03, "Tv",250,0.15, 10, 759352);
        produto.add(produto1);
        produto.add(produto2);
        produto.add(produto3);
    }
    @GetMapping
    public List<Produtos> findAll(@RequestParam(required = false) Produtos produtos){
        if(produtos.getNome() != null){
            System.out.println("Código do produto: " + produtos.getCodigodoproduto());
            System.out.println( produtos.getNome() + " Comprada com sucesso.");
            System.out.println( "O item foi comprado usando o cupom de desconto e o valor final ficou " + produtos.CalcularValorDesconto(produtos.getValor(), produtos.getDesconto()));
        }
        if(produtos.getQuantidadenoestoque() <= produtos.getQuantidadenoestoque()){
            System.out.println(produtos.getQuantidadenoestoque() + produtos.getNome() + " comprada com sucesso.");
            System.out.println("O valor total deu: " + produtos.CalcularQuantidadeComprada(produtos.getQuantidadenoestoque(), produtos.getValor()));

        }
        if(produtos.getQuantidadenoestoque() > produtos.getQuantidadenoestoque()){
            System.out.println("Produto esgotado.");
        }
        return produto;
    }
    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody final Produtos produtos){
        if (produtos.getId() == null) {
            produtos.setId(produto.size() + 1);
        }
        produto.add(produtos);
        return new ResponseEntity<>(produtos.getId(), HttpStatus.CREATED);
    }
}
